cmake_minimum_required(VERSION 2.8)
project(tdd)

include(../common.cmake)

add_executable(test_tdd
  test.cpp
  ../commons/catch_main.cpp)
