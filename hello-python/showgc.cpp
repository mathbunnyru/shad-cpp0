#define Py_LIMITED_API
#include <Python.h>

#include <vector>

static PyObject* showgc_save(PyObject *self, PyObject *args);
static PyObject* showgc_load(PyObject *self, PyObject *args);

static PyMethodDef ShowgcMethods[] = {
    {"save", showgc_save, METH_VARARGS, "Keep reference to object."},
    {"load", showgc_load, METH_VARARGS, "Return saved object."},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

static PyModuleDef showgcmodule = {
    PyModuleDef_HEAD_INIT,
    "showgc",
    "Ref counting example",
    -1,
    ShowgcMethods
};

PyMODINIT_FUNC PyInit_showgc() {
    return PyModule_Create(&showgcmodule);
}

static std::vector<PyObject*> Keep;

static PyObject* showgc_save(PyObject *self, PyObject *args) {
    Keep.push_back(args);
    Py_INCREF(Keep.back());

    Py_RETURN_NONE;
}

static PyObject* showgc_load(PyObject *self, PyObject *args) {
    int index;
    if (!PyArg_ParseTuple(args, "i", &index)) {
        return NULL;
    }

    if (index < 0 || index >= Keep.size()) {
        PyErr_SetString(PyExc_RuntimeError, "Out of range");
        return NULL;
    }

    Py_XINCREF(Keep[index]);
    return Keep[index];
}
